import UserAlert from '../components/UserAlert.vue'
export default {
    data() {
      // Important: if we have the same property for example alertIsVisible in the components who are using this mixing
       // then: the component properties overwrite the mixing

      return {
        alertIsVisible: false,
      };
    },
    methods: {
      showAlert() {
        this.alertIsVisible = true;
      },
      hideAlert() {
        this.alertIsVisible = false;
      }
    },
};
