# README #

Project to understand how mixins functions work using Composition API


### What will I learn?? ###

* Undestanding mixins mergin
* Using global Mixins


### Important ###
* If we have the same property for example alertIsVisible in the components who are using this mixing, then: the component properties overwrite the mixing

* if we want to create a Global muxing we must setup in main.js, in the way below listed: 
* 1. import testMixing from 'mypath'
* 2. app.mixing(testMixing)
